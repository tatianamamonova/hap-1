package bitset

// Bitset is fixed-size sequence of `size` bits
type Bitset struct {
}

// New creates a new Bitset of a given size
func New(size int) *Bitset { return nil }

// Set sets a specific bit
func (b *Bitset) Set(pos int, value bool) error {
	return nil
}

// Test returns a value of specific bit
func (b *Bitset) Test(pos int) (bool, error) {
	return false, nil
}

// Count is returns the number of bits set to `true`
func (b *Bitset) Count() int {
	return -1
}

// All checks if all bits are set to `true`
func (b *Bitset) All() bool {
	return false
}

// Any checks if there is at least one bit set to `true`
func (b *Bitset) Any() bool {
	return false
}

// Flip toggles the values of bits
func (b *Bitset) Flip() {
}

// Reset sets all bits to `false`
func (b *Bitset) Reset() {
}
