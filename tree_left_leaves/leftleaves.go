package leftleaves

// A Tree is a binary tree with integer values.
type Tree struct {
	Value int
	Left  *Tree
	Right *Tree
}

// SumOfLeftLeaves returns the sum of left leaves values for the given tree
func SumOfLeftLeaves(tree *Tree) int {
	return 0
}
