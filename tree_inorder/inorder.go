package inorder

// A Tree is a binary tree with integer values.
type Tree struct {
	Value int
	Left  *Tree
	Right *Tree
}

// Inorder returns an inorder traversal of the given tree
func Inorder(tree *Tree) []int {
	return []int{}
}
