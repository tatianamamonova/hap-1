package tree

// A Tree is a binary tree with integer values.
type Tree struct {
	Value int
	Left  *Tree
	Right *Tree
}

// New creates a binary tree from the array representation
func New(data []string) (*Tree, error) {

	// Your code goes here

	return nil, nil
}

// Serialise returns a normalised array representation of the given tree
func Serialise(tree *Tree) []string {

	// Your code goes here

	return []string{}
}
