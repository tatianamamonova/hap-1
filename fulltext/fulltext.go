package fulltext

// Index implements fulltext search
type Index struct{}

// New creates a fulltext search index for the given documents
func New(docs []string) *Index {
	return nil
}

// Search returns a slice of unique ids of documents that contain all words from the query.
func (idx *Index) Search(query string) []int {
	return []int{}
}
