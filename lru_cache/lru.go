package lru

// Cache represents an fixed-size LRU cache for integer keys and values
type Cache struct {
}

// New returns an initialised LRU cache for the given capacity
func New(capacity int) *Cache {
	return nil
}

// Get returns a cached value for the given key, or -1 if the key does not exist
func (cache *Cache) Get(key int) int {
	return -1
}

// Put inserts or updates the value for the given key.
// When the cache capacity is reached, it removes the least recently used item before inserting a new one.
func (cache *Cache) Put(key int, value int) {

}
