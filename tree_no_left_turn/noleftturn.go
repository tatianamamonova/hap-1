package noleftturn

// A Tree is a binary tree with integer values.
type Tree struct {
	Value int
	Left  *Tree
	Right *Tree
}

// NoLeftTurn transforms a binary search tree into an equivalent one that has no left subtrees.
// Returns a pointer to the new root.
func NoLeftTurn(tree *Tree) *Tree {
	return tree
}
