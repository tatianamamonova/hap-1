package ndarray

// NDArray is a prototype of an n-dimensional array
type NDArray struct {
	shape []uint32
}

// New creates an NDArray with given dimensions
func New(shape ...uint32) *NDArray {
	ndarr := new(NDArray)
	ndarr.shape = shape
	return ndarr
}

// Idx an index in a linear array for a given n-dimensional index
func (nda *NDArray) Idx(indicies ...uint32) (uint32, error) {

	// ... here goes your code ...

	return 0, nil
}
